#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.shortcuts import render,render_to_response
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from myapp.models import testsample


# Create your views here.

def add(request):
    return render(request, 'main.html')

@csrf_exempt
def back(request):
    if request.method == 'POST':
        print(request.POST.get('name'))
        print(request.POST.get('id'))
        b = testsample(n_name=request.POST.get('name'),n_id=request.POST.get('id'))
        b.save()
        return HttpResponse("post")
    else:
        return HttpResponse("GET request")


def retrieve(request):
    profiles = testsample.objects.all()
    return render_to_response('retreive.html',{'profiles':profiles})
