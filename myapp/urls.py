from django.conf.urls import url, include
import views

urlpatterns = [
    url(r'add/$', views.add),
    url(r'back/$', views.back),
    url(r'retrieve/$', views.retrieve)
]
